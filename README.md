# Desafio da auditoria financeira

Uma empresa de auditoria financeira te contratou para fazer o sistema de birô deles. Eles querem filtrar as empresas que fazem cadastro no site deles por capital, empresas com capital acima de 1Mi são aceitas e as com menos são recusadas.

Eles pediram para dividir o sistema em 3 microsserviços: 
 - O primeiro é onde a empresa se cadastram com seu CNPJ via API.
 - O segundo recebe os cadastros via Kafka e valida o capital via API do governo.
 - O terceiro recebe o resultado do cadastro das empresas e o salva em um arquivo CSV

A API de consulta do capital do governo é:
https://consultarcnpj.com.br/wp-content/plugins/cnpj-rf/receita-ws.php?cnpj=(cnpj sem pontuação)

## Detalhes:
Foram criados os seguintes tópicos no Kafka:
 - leandro-biro-1
 - leandro-biro-2
 - kaique-biro-1
 - kaique-biro-2
 - pedro-biro-1
 - pedro-biro-2
 - luis-biro-1
 - luis-biro-2
 - chrislucas-biro-1
 - chrislucas-biro-2
 - vittoria-biro-1
 - vittoria-biro-2

O ip do servidor de kafka é: **54.39.28.62:9092**